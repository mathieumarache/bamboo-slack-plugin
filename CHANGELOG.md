# CHANGELOG

## 1.5.11

2021/11/05

- Make plugin compatible with Bamboo 8.0 DataCenter thanks to Alexander Over

## 1.5.10

2020/09/16

- Corrected conflict in ftl template keys thanks to Alexey Chystoprudov.
- Updated bamboo and amps version thanks to Evgeni Gordeev.

## 1.5.9