[@ww.textfield labelKey="slack.webhookUrl" name="slack_webhookUrl" value="${slack_webhookUrl!}" required='true'/]
[@ww.textfield labelKey="slack.channel" name="slack_channel" value="${slack_channel!}" required='false'/]
[@ww.textfield labelKey="slack.iconUrl" name="slack_iconUrl" value="${slack_iconUrl!}" required='false'/]
[@ww.textfield labelKey="slack.botName" name="slack_botName" value="${slack_botName!}" required='false'/]
